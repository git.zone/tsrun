// node native
import * as path from 'path';
import * as url from 'url';

export { path, url };

// @pushrocks scope
import * as smartshell from '@push.rocks/smartshell';

export { smartshell };

// third party scope
import * as tsNode from 'ts-node';

export { tsNode };
