/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@gitzone/tsrun',
  version: '1.2.44',
  description: 'run typescript programs efficiently'
}
